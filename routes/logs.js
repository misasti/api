var express 	= require('express'),
	router 		= express.Router(),
	sqlite3 	= require('sqlite3').verbose(),
	db 			= new sqlite3.Database('./time-keeper.db'),
	cors 		= require('cors'),
	table 		= 'logs',
	async		= require('async'),
	dateformat	= require('dateformat');



var check;
var datass;

router.use(cors());

/* GET users listing. */
router.get('/', function(req, res, next) {
	
	// var empid = req.params.EMPLOYEE_ID;

	 db.serialize(function() {
	 	db.all(" SELECT rowid as id,* FROM logs", function(err, rows) {
	    	if (err) {
	            throw err;
	        }
	        console.log('length:'+rows.length)
	        /*var message = "Viewing " 
	             + 'time-keeper.db' + '/' + table +".";*/
	        console.log(rows)
	        res.json(rows);
	    });
	  });
	    
  
});

/* GET users listing. */
router.get('/search/:employeeName?/:ldate?/:empid?', function(req, res, next) {
	var empid 		= req.params.empid,
		empname		= req.params.employeeName,
		ldate 		= req.params.ldate,
		sql,
		sqlemp,
		sqldate,
		sqlname;
	
	if (empid == "" || empid == undefined || empid == 0) {
  		sqlemp = ''
  	} else{
  		sqlemp = " AND logs.employeeId = "+empid
  		// sql = sql+sqlemp
  	}
  	if (empname == "" || empname == undefined || empname == 'none') {
  		sqlname = ''
  	}else{
  		sqlname = " (users.firstname LIKE '"+empname+"%' OR lastname LIKE '"+empname+"%')"
  		// sql = sql+sqlname
  	}

  	if (ldate == "" || ldate == undefined || ldate == 0) {
  		sqldate = ''
  	}else{
  		ldate = dateformat(ldate,'mm/dd/yyyy')
  		sqldate = " AND logDate ='"+ldate+"'"
  		// sql = sql+sqldate
  	}
	console.log("ldate=="+ldate)
	db.serialize(function() {
		sql = "SELECT logs.rowid as logId, logs.employeeId, (users.firstname || ' ' || users.lastname) AS employeeName, logs.logTime AS logTime, logs.logDate AS logDate FROM logs INNER JOIN users ON logs.employeeId = users.employeeId";

		if (empid || empname || ldate) {
			console.log("pasok here=="+sqlname)

			sql = sql+" WHERE "+sqlname+sqldate+sqlemp
			// console.log(sqlname)
		};

		console.log(sql)
	 db.all(sql, function(err, rows) {
	    	if (err) {
	            throw err;
	        }
	        console.log('length:'+rows.length)
	        var message = "Viewing " 
	             + 'time-keeper.db' + '/' + table +".";
	        console.log(rows)
	        res.json(rows);
	    });
	  });
});


/*
 * For individual
 */
router.get('/solosearch/:empid/:ldate?', function(req, res, next) {
	var empid 		= req.params.empid,
		// empname		= req.params.employeeName,
		ldate 		= req.params.ldate,
		sql,
		sqlemp,
		sqldate,
		sqlname;

  	if (ldate == "" || ldate == undefined || ldate == 0) {
  		sqldate = ''
  	}else{
  		ldate = dateformat(ldate,'mm/dd/yyyy')
  		sqldate = " AND logDate ='"+ldate+"'"
  		// sql = sql+sqldate
  	}

	console.log("ldate=="+ldate)
	db.serialize(function() {
		sql = "SELECT * FROM logs WHERE employeeId = "+empid;

		if (ldate) {
			console.log("pasok here=="+sqlname)

			sql = sql+sqldate
			// console.log(sqlname)
		};

		console.log(sql)
	 db.all(sql, function(err, rows) {
	    	if (err) {
	            throw err;
	        }
	        console.log('length:'+rows.length)
	        var message = "Viewing " 
	             + 'time-keeper.db' + '/' + table +".";
	        console.log(rows)
	        res.json(rows);
	    });
	  });
});

function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

/**
 * Instead of putting the value in a parameter, it could be passed on through json object
 */
router.post('/insert', function(req, res, next) {
	var empid 	=	req.body.empid,
		ldate 	=	req.body.date,
		ltime	=	req.body.time;

	var data;
	var isflag = 0;
	var d= new Date();
  	var n = d.toLocaleString();
  	var dd = d.toLocaleDateString();

  	var h = addZero(d.getHours());
    var m = addZero(d.getMinutes());
    var s = addZero(d.getSeconds());
    var time = h + ":" + m + ":" + s;

    console.log("start")

    async.series(
    	[
    		function isExisting(callback){
    			var sqlEmpid = "SELECT isFlag FROM logs WHERE employeeId = '"+empid+"' AND logDate = '"+ldate+"' ORDER BY rowid DESC LIMIT 1";
    			db.each(sqlEmpid, 
					function(err, emprow) {
					if (emprow.isFlag == 1){
						isflag = 2;
						console.log("emprow.isFlag=="+isflag)
					}
					else { 
						isflag = 1;
						console.log("emprow.isFlag=="+isflag)
					}
					return callback()
					console.log('emprow='+emprow.isFlag)
				}, function(err, rows){
					if(rows == 0){
						isflag = 1;
						console.log("isflag=="+isflag)
						return callback()
					}
				})
    			console.log("isflag=="+isflag)
				// return callback();
    		},
    		function insertLogs(callback){
    			db.serialize(function() {

					db.run("begin transaction");
				    var stmt = db.prepare("INSERT INTO logs VALUES (?,?,?,?,?)");

				    // stmt.run(155,dd,time,n);
				    stmt.run(empid, ldate, ltime,isflag, n);
				    stmt.finalize();
				    db.run("commit");

				    return callback()
			 	});
    		}
    	], function (err){
    		if (err) return res.json({success: false, data: {}, errors: err, message: "An error occured"});

			res.json({success: true, data: data, errors: {}, message: "Successfully saved logs"});
    	}
    )

	/*var sqlEmpid = "SELECT isFlag FROM logs WHERE employeeId = '"+empid+"' AND logDate = '"+ldate+"' ORDER BY rowid DESC LIMIT 1";
	
	db.serialize(function() {
		db.each(sqlEmpid, 
			function(err, emprow) {
			if (emprow[0].isFlag == 1){
				isflag = 2;
				console.log("emprow.isFlag=="+isflag)
			}
			else { 
				isflag = 1;
				console.log("emprow.isFlag=="+isflag)
			}
			console.log('emprow='+emprow[0].isFlag)
		}, function(err, rows){
			if(rows == 0){
				isflag = 1;
				console.log("isflag=="+isflag)
				return rows
			}
		})
	});
	console.log("labas=="+isflag)
		var errmsg;

		if(isflag != 0){
			db.serialize(function() {

				db.run("begin transaction");
			    var stmt = db.prepare("INSERT INTO logs VALUES (?,?,?,?,?)");

			    // stmt.run(155,dd,time,n);
			    stmt.run(empid, ldate, ltime,isflag, n);
			    stmt.finalize();
			    db.run("commit");
			    

			 });

			res.json({success:true, message:"Your log is successfully saved!"})
		}
	*/
				

	
});

/*
 *
 */
router.get('/delete/:id', function(req, res, next) {

	

	// var params = req.body; -insert if param is available
	db.serialize(function() {
		
		db.run("begin transaction");
		var rid = req.params.id;
		var sql = "DELETE FROM logs WHERE rowid = "+ rid;

		if(rid != '' || rid != null){
			db.run(sql);
		}
		db.run("commit");
		console.log("success logs delete")
		res.json({success: true, data: {}, errors: {}, message: "Your log has been deleted!"});
	});

});

router.get('/log', function(req, res, next) {

  res.send('respond with a resource');
});

module.exports = router;
