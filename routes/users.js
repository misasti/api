var express 	= require('express'),
	router 		= express.Router(),
	sqlite3 	= require('sqlite3').verbose(),
	db 			= new sqlite3.Database('./time-keeper.db'),
	cors 		= require('cors'),
	crypto		= require('crypto'),
	async		= require('async');

var check;
var datass;

router.use(cors());

/* GET users listing. */
router.get('/all', function(req, res, next) {
	var table = 'users';
  
	 db.serialize(function() {
	    db.all(" SELECT rowid as id, * FROM " + table, function(err, rows) {
	    	if (err) {
	            throw err;
	        }
	        console.log('length:'+rows.length)
	        var message = "Viewing " 
	             + 'time-keeper.db' + '/' + table +".";
	        console.log(rows)
	        res.json(rows);
	    });
	  });
  
});

/* GET users listing. */
/*router.get('/search/:fname?/:lname?/:empid?', function(req, res, next) {
	var empid = req.params.empid,
		fname = req.params.fname,
		lname = req.params.lname,
		qsql,
		sqlemp,
		wparam;
	console.log(empid)
  	
  	if (empid == "" || empid == undefined) {
  		sqlemp = ''
  	} else
  		sqlemp = " AND employeeId = "+empid
  	if (fname == "" || fname == undefined) {
  		fname = ""
  	}
  	if (lname == "" || lname == undefined) {
  		lname = ""
  	}

  	qsql = fname+lname+empid;

	 db.serialize(function() {
	 	var sql = "SELECT * FROM users ";

	 	if (empid || fname || lname) {
	 		sql = sql+"WHERE firstname LIKE '"+fname+"%' AND lastname LIKE '"+lname+"%'"+sqlemp;
	 	}
	 		
	 	console.log(sql)
	    db.all(sql, function(err, rows) {
	    	if (err) {
	            throw err;
	        }
	        console.log('length:'+rows.length)
	        console.log(rows)
	        res.json(rows);
	    });
	  });
  
});*/
/* GET users listing. */
router.get('/searches/:limit/:startpage/:term?/:sort', function(req, res, next) {
	var limit = req.params.limit,
		start = req.params.startpage,
		term = req.params.term,
		qsql,
		userCnt,
		rdata;
	// console.log(empid)
  	
  	if (term == "" || term == undefined) {
  		sqlterm = ''
  	}
	

	 db.serialize(function() {
	 	var getAllcount = "SELECT count(*) as count FROM users";
  		db.each(getAllcount, function (err, data){
  			userCnt = data
  			console.log(data.count)
  		})

	 	var sql = "SELECT * FROM users ";

	 	if (term) {
	 		sql = sql+"WHERE firstname LIKE '"+term+"%' AND lastname LIKE '"+term+"%'";
	 	}
	 		
	 	console.log(sql)
	    db.all(sql, function(err, rows) {
	    	if (err) {
	            throw err;
	        }
	        console.log('length:'+rows.length)
	        console.log(rows)

	        rdata={
	        	"draw": 1,
	        	"recordsTotal": userCnt,
			    "recordsFiltered": rows.length,
			    "data": rows
	        }

	        res.json(rows);
	    });
	  });
  
});

/*
 * Values passed on to this endpoint may not necessarily be put into parameters together with the url
 * Values can be passed on by json object or html object
*/
router.post('/insert', function(req, res, next) {
	var empid 		= req.body.employeeId,
		fname 		= req.body.firstname,
		mname 		= req.body.middlename,
		lname 		= req.body.lastname,
		username 	= req.body.username,
		password 	= req.body.password,
		roleId		= req.body.roleId;
		console.log('username='+username);
	var data = [];

	async.series(
		[
			function usernameExist(callback) {
				var querysql = "SELECT count(*) AS count FROM users WHERE username = '"+username+"'";

				db.each(querysql, function(err, row) {
			      // console.log(row);
					if (row.count > 0) 
						return callback("Username is already existing.");

					return callback();
				})	
					
			},
			function empidExist(callback) {

				var sqlEmpid = "SELECT count(*) AS count FROM users WHERE employeeId = '"+empid+"'";
				
				db.each(sqlEmpid, function(err, emprow) {
						if (emprow.count > 0)
							return callback("Employee ID is already existing.");

						return callback();
					})
			},
			function insert(callback) {

				crypto.pbkdf2(password, 'salt', 100000, 22, 'sha256', (err, key)=> {
					if (err) return callback(err);

					var passwordHash = key.toString('hex');
					password = passwordHash;

					var errmsg;

					db.serialize(function() {
		
					// var params = req.body; -insert if param is available
						db.run("begin transaction");
					    var stmt = db.prepare("INSERT INTO users VALUES (?,?,?,?,?,?,?,?,?,?,?)");
					    var d= new Date();
					  	var n = d.toLocaleTimeString();

					    stmt.run(empid,fname,mname,lname,username,password,1,null,roleId,n,null);

					    stmt.finalize();
					   /* stmt.on('error', function(err) {
					    	errmsg = err
					    	return errmsg
					    })*/
					    // res.render("Success")

					    db.run("commit");
					    
					 });

					db.serialize(function() {
						var queryGet = "SELECT employeeId,firstname,middlename,lastname,status,roleId FROM users WHERE employeeId = '"+empid+"'";
					    db.all(queryGet, function(err, emprow) {
					    	console.log(emprow.length)
							if (emprow.length == 0)
								return errmsg = ("Employee details not saved.");
							else { 
								data.push(emprow)
							}
							return callback();
						})


					
					})

				});
			}		
		],
		function (err) {
			if (err) return res.json({success: false, data: {}, errors: err, message: "An error occured"});

			res.json({success: true, data: data, errors: {}, message: "Successfully created a user"});
		}
	);
	
});


function tx_handler(callback, commit) {
    return function() {
        var args = Array.prototype.slice.call(arguments);
        var err = args[0];
        if(err)  db.run('rollback');             
        else if(commit) db.run('commit');
        if(callback)
            callback.apply(this, args);
    }
}

router.post('/update/:id', function(req, res, next) {

	var userid = req.params.id;
	var empid = req.body.empid;
	var fname = req.body.fname;
	var lname = req.body.lname;
	var mname = req.body.mname;
	var uname = req.body.uname;
	var roleid = req.body.roleid;
	var data = [];
	async.series(
		[
			function isExisting(callback){
				var sqlexist = "SELECT count(*) AS count FROM users WHERE rowid != "+userid+" AND username='"+uname+"'";
				console.log(sqlexist)
				db.each(sqlexist, function(err, row) {
			      console.log(row);
					if (row.count > 0) {
						console.log("pasok")
						return callback("Username is already existing.");
					}

					return callback();
				})	
			} 
			,function statsupdate(callback){
				var sql = "UPDATE users SET employeeId = "+empid+",firstname = '"+fname+"',lastname = '"+lname+"',middlename = '"+mname+"',username = '"+uname+"',roleId = "+roleid+" WHERE rowid = "+userid;
				db.serialize(function(err) {
				    	db.run(sql);
				    	return callback()
				});
				
			}
		],function (err) {
			if (err) return res.json({success: false, errors: err, message: "An error occured"});

			res.json({success: true, errors: {}, message: "Successfully changed user details"});
		}
	);

	// var params = req.body; -insert if param is available
	 /*db.serialize(function(err) {
	    if(userid != '' || userid != null){
	    	db.run(sql);
	    }

	    if(err)
       	 throw err;
    	else res.json({message: "Success on updating user"});
	  });
  
    console.log(res);*/
});

router.post('/delete/:id', function(req, res, next) {

	var userid = req.params.id;
	var sql = "DELETE FROM users WHERE rowid = "+ userid;

	// var params = req.body; -insert if param is available
	 db.serialize(function(err) {
	    if(userid != '' || userid != null){
	    	db.run(sql);
	    }

	    if(err)
       	 throw err;
    	else res.json({message: "Success on deleting user"});
	  });
  
    console.log(res);
});


router.get('/status/:id', function (req, res) {

  if (req.params.id != undefined && req.params.id != '') {

  	var empid = req.params.id;
  	var data;
	db.serialize(function() {
	 db.each(" SELECT status FROM users WHERE rowid = " + empid, function(err, rows) {
	    	if (err) {
	            throw err;
	        }

	        if (rows.status == 1) {

	        	res.json({active: true})
	        } else{
	        	res.json({active: false})
	        };

	    });
	  });
  }
}); 

router.post('/changeStatus', function (req, res, next) {

  	var empid = req.body.empid;
  	var stat = req.body.stat;
  	var data = [];
  	console.log(stat)

  	/*async.each(
		[
			function validate(callback){
				if (empid == undefined || stat == undefined) 
					return callback("Parameters are undefined")
			} 
			,function statsupdate(callback){
				if (empid != undefined && stat != undefined) 
				{
					console.log("pasok")
					var sql = "UPDATE users SET status ="+stat+" WHERE employeeId = "+empid;
					
				 	db.serialize(function(err) {
				    	db.run(sql);
				    	data.push({status: stat})
					    	
					    return callback();
				  	});
			 	}
			}
		],function (err) {
			if (err) return res.json({success: false, data: {}, errors: err, message: "An error occured"});

			res.json({success: true, data: data, errors: {}, message: "Successfully changed user status"});
		}
	);*/

	var sql = "UPDATE users SET status = "+stat+" WHERE employeeId = "+empid;

	// var params = req.body; -insert if param is available
	 db.serialize(function(err) {
	    if(empid != undefined && stat != undefined){
	    	db.run(sql);
	    	data.push({employeeId: parseInt(empid), status: stat})
	    	res.json({success: true, data: data, error:{}, message: "Success on updating user"});
	    }
    	else res.json({success: false, data: {}, error:"Undefined values.", message: "Parameters error"})
	  });
  
    // console.log(res);
}); 

router.post('/changePass', function (req, res, next) {

  	var empid = req.body.empid;
  	var password = req.body.password;
  	var data = [];

  	crypto.pbkdf2(password, 'salt', 100000, 22, 'sha256', (err, key)=> {
		if (err) return callback(err);

		var passwordHash = key.toString('hex');
		password = passwordHash;


		var sql = "UPDATE users SET password = "+password+" WHERE employeeId = "+empid;

		// var params = req.body; -insert if param is available
		 db.serialize(function(err) {
		    if(empid != undefined && pass != undefined){
		    	db.run(sql);
		    	res.json({success: true, error:{}, message: "Success on updating your password"});
		    }
	    	else res.json({success: false, error:err, message: "Error encountered while saving password."})
		  });
	});

  
    // console.log(res);
});

router.post('/updatePhoto', function (req, res, next) {

  	var empid = req.body.empid;
  	var picpath = req.body.photoPath;
  	var data = [];

	var sql = "UPDATE users SET profilePic = "+picpath+" WHERE employeeId = "+empid;

	// var params = req.body; -insert if param is available
	 db.serialize(function(err) {
	    if(empid != undefined && picpath != undefined){
	    	db.run(sql);
	    	res.json({success: true, error:{}, message: "Success on updating your photo"});
	    }
    	else res.json({success: false, error:err, message: "Error encountered while saving phote."})
	  });
  
    // console.log(res);
});

router.get('/log', function(req, res, next) {

  res.send('respond with a resource');
});

module.exports = router;
