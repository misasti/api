var express 	= require('express'),
	router 		= express.Router(),
	sqlite3 	= require('sqlite3').verbose(),
	db 			= new sqlite3.Database('./time-keeper.db'),
	cors 		= require('cors'),
	crypto		= require('crypto'),
	async		= require('async');

var check;
var datass;

router.use(cors());


/*
 * <!--BEGIN CRUD FOR ROLES -->
 *
*/

/* GET roles listing. */
router.get('/all', function(req, res, next) {
	// var rid = req.params.id;
  
	 db.serialize(function() {
	    db.all(" SELECT rowid as id,roleName FROM roles", function(err, rows) {
	    	if (err) {
	            throw err;
	        }
	        console.log('length:'+rows.length)
	        console.log(rows)
	        res.json(rows);
	    });
	  });
  
});

/*
 *
 * Values passed on to this endpoint may not necessarily be put into parameters together with the url
 * Values can be passed on by json object or html object
*/
router.post('/insert', function(req, res, next) {
	var roleName 	= req.body.roleName,
		permission 	= req.body.permission;
	var data = [];	
		
	async.series(
		[
			function isExisting(callback) {
				var querysql = "SELECT count(*) AS count FROM roles WHERE roleName = '"+roleName+"'";
				db.each(querysql, function(err, row) {
			      console.log(row);
					if (row.count > 0) return callback("Role name already exists");

					return callback();
				})
			},
			function insert(callback) {

				db.serialize(function() {
	
				// var params = req.body; -insert if param is available
					db.run("begin transaction");
				    var stmt = db.prepare("INSERT INTO roles VALUES (?,?,?)");
				    var d= new Date();
				  	var n = d.toLocaleTimeString();

				    stmt.run(roleName,permission,n);
				    stmt.finalize();
				    // res.render("Success")

				    db.run("commit");
				    db.all("SELECT * FROM roles WHERE roleName ='"+roleName+"'", function(err, rows) {
				    	if (err) {
				            throw err;
				        }
				        console.log('length:'+rows.length)
				        console.log(rows)
				        data.push(rows);

				        return callback();
				    });
				    
				 });
			}	
		],
		function (err) {
			if (err) return res.json({success: false, data: data, errors: err, message: "An error occured"});

			res.json({success: true, data: data, errors: {}, message: "Successfully created a role"});
		}
	);
	
});


router.post('/update/:id', function(req, res, next) {

	var roleId 		= req.params.id,
		rolename 	= req.body.rolename,
		permit		= req.body.permission;
	var data = [];
	async.series(
		[
			function isExisting(callback){
				var sqlexist = "SELECT count(*) AS count FROM roles WHERE rowid != "+roleId+" AND roleName='"+rolename+"'";
				console.log(sqlexist)
				db.each(sqlexist, function(err, row) {
			      console.log(row);
					if (row.count > 0) {
						console.log("pasok")
						return callback("Role name is already existing.");
					}

					return callback();
				})	
			} 
			,function roleupdate(callback){
				var sql = "UPDATE roles SET roleName = '"+rolename+"', permission = '"+permit+"' WHERE rowid = "+roleId;
				db.serialize(function(err) {
				    	db.run(sql);
				    	return callback()
				});
				
			}
		],function (err) {
			if (err) return res.json({success: false, errors: err, message: "An error occured"});

			res.json({success: true, errors: {}, message: "Successfully updated role details"});
		}
	);	

});

/*
 *
 */
router.post('/delete/:id', function(req, res, next) {

	var roleid = req.params.id;
	var sql = "DELETE FROM roles WHERE rowid = "+ roleid;

	// var params = req.body; -insert if param is available
	 db.serialize(function(err) {
	    if(roleid != '' || roleid != null){
	    	db.run(sql);
	    }

	    if(err)
       	 throw err;
    	else res.json({message: "Success on deleting role"});
	  });
  
    console.log(res);
});


/*
 * <!-- END CRUD FOR ROLES -->
 *
 */

 module.exports = router;