var express 	= require('express'),
	router 		= express.Router(),
	sqlite3 	= require('sqlite3').verbose(),
	db 			= new sqlite3.Database('./time-keeper.db'),
	cors 		= require('cors'),
	jwt         = require('jwt-simple'),
	async		= require('async'),
    crypto      = require('crypto'),
    session     = require('express-session'),
    bodyParser  = require('body-parser'),
    sess,
    conn        = require('./config'),
    sys			= require('sys'),
    passport	= require('passport'),
    LdapStrategy = require('passport-ldapauth');

/*
 * LDAP Authentication
 */


router.use(cors());
router.use(session({secret: 'ssshhhhh',saveUninitialized: true,resave: true}));
router.use(bodyParser.json());      
router.use(bodyParser.urlencoded({extended: true}));


router.get('/login', function (req, res){
	//Session set 
  sess = req.session;

  //if session exist will do an action
  if(sess.uname){
    res.redirect('/'); //render index path here

    console.log('session:'+sess);
  }else{
    // res.render('login');
    console.log('session:'+JSON.stringify(sess));
  }
});

/**
 * @api {POST} /api/auth/login Authenticate user during login
 * @apiName Login
 * @apiGroup Login
 *
 * @apiSuccessExample Success-Response:
 *  {
 *    success: boolean,
 *    data: object,
 *    errors: object,
 *    message: "success message"
 *    
 *  }
 *
 * @apiExample {curl} Example usage:
 * http://202.90.158.128:3000/api/login
 */
router.post('/login/native', function(req, res, next){
    var post = req.body,
        sess = req.session;
    var password;
    var username = post.username;

    crypto.pbkdf2(post.password, 'salt', 100000, 22, 'sha256', (err, key) => {
		var password = key.toString('hex');
		var data = [];
		db.serialize(function() {

	    	var querysql = "SELECT count(*) as cnt, * FROM users WHERE username = '"+post.username+"' AND password = '"+password+"' LIMIT 1;";
	    	// var querysql = "select key,value from users WHERE username = '"+post.username+"' AND password = '"+password+"' LIMIT 1";
			db.each(querysql, function (err, row) {
		      
		      console.log(row.username)

		      // console.log(row.length);
				/*if (row.count > 0) return callback("Username is already existing");*/

				if (row.cnt > 0) 
				{
	                data.push({firstname: row.firstname})
                  	var roleFlags = {
	                    isSuperAdmin: false,
	                    isAdministrator: false,
	                    isViewer: false
                    };
	                  
	                  async.series(
	                    [
	                    	function isSuperAdmin(callback)
	                    	{
	                    	
						    	var querysql = "SELECT roleName FROM roles WHERE rowid = "+row.roleId;
								db.each(querysql, function(err, row) {
							      console.log(row.roleName);

									if (Object.keys(row).length > 0) {
	                                    if (row.roleName == 'Super Administrator') {
	                                        roleFlags.isSuperAdmin = true;
	                                    } else if (row.roleName == 'ADMINISTRATOR') {
	                                        roleFlags.isAdministrator = true;
	                                    } else if (row.roleName == 'VIEWER') {
	                                        roleFlags.isViewer = true;
	                                    }
	                                    return callback();
	                                }
									

								})
						  		
	                         	
	                        }
	                      
	                    ], function (err) {
	                      if (err) return res.json({success: false, data: [], errors: err, message: 'An error has occured'});
	                      
	                      data.push(roleFlags);
	                      res.json({success: true, data: data, errors: {}, message: "Successful login"});
	                    }
	                  );
		                   	
		       } else {

		            return res.json({success: false, data: {}, errors: {}, message: "The email and password you entered do not match"});
		        }

			
			})
		}) //end of db.serialize
	}); //end of crypto

});


router.post('/login/ldap',function(req, res, next) {
  console.log(req.body.username)
  var	password = req.body.password,
  		username = req.body.username;
  var OPTS = {
	  server: {
	    url: 'ldap://accounts.asti.dost.gov.ph:389',
	    // bindDn: 'cn=users,cn=accounts,dc=asti,dc=dost,dc=gov,dc=ph',
	    bindDn: 'cn='+username+', cn=accounts,dc=asti,dc=dost,dc=gov,dc=ph',
	    bindCredentials: 'password',
	    searchBase: 'ou=users',
	    searchFilter: '(&(uid={{username+}})(objectClass=person))'
	  }
	};
	console.log(OPTS)
	passport.use(new LdapStrategy(OPTS));

   passport.authenticate('ldapauth', {session: false}, function(err, user, info) {

    if (err) {
      return next(err); // will generate a 500 error
    }
    // Generate a JSON response reflecting authentication status
    if (! user) {
      return res.send({ success : false, message : 'authentication failed' });
    }
    return res.send({ success : true, message : 'authentication succeeded' });
  })(req, res, next);
});


/**
 * @api {POST} /api/auth/logout Logs out user from the application
 * @apiName Login
 * @apiGroup Login
 *
 * @apiSuccessExample Success-Response:
 *  {
 *    constraints: ["column","names","read",from","primary","document"],
    docuri: "documentUri.json/match",
 *    primary_data: {"uri": "file"},
 *    matches: {"uri":"file","columns": "value"}
 *    
 *  }
 *
 * @apiExample {curl} Example usage:
 * http://202.90.158.128:3000/api/dynamicSelect?uri=uri
 */
router.get('/logout', function (req, res) {
	// var empid = req.params.id;
  req.session.destroy(function(err) {
    if(err) {
      console.log(err); //or res.send(404) or error message: res.JSON({success:false, message: "Error on logging out user"}); //or res.send(404) or error message
    } else {
      res.json({success: true, message: "Logout successfully"});
    }
  });
  
});


module.exports = router;
