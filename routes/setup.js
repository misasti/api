var express = require('express');
var router = express.Router();
var sqlite3 = require('sqlite3');
var TransactionDatabase = require("sqlite3-transactions").TransactionDatabase;
var cors = require('cors');

var check;
var datass;

var db = new TransactionDatabase(
    new sqlite3.Database("time-keeper.db", sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE)
);

router.use(cors());

router.get('/newTbl', function(req,res) {
	// Use db as normal sqlite3.Database object.
	db.exec("CREATE TABLE if not exists users (EMPLOYEE_ID INT, FNAME VARCHAR(50),MNAME VARCHAR(50),LNAME VARCHAR(50),DATE_UPDATED DATETIME)", function(err) {
		// table created
	});

	// Begin a transaction.
	db.beginTransaction(function(err, transaction) {
		// Now we are inside a transaction.
		// Use transaction as normal sqlite3.Database object.
		transaction.run("INSERT ...");


		// All calls db.exec(), db.run(), db.beginTransaction(), ... are
		// queued and executed after you do transaction.commit() or transaction.rollback()
		
		// This will be executed after the transaction is finished.
		database.run("INSERT ..."); 
	    
	    // Feel free to do any async operations.
	    someAsync(function() {
	    
	        // Remember to .commit() or .rollback()
		    transaction.commit(function(err) {
	            if (err) return console.log("Sad panda :-( commit() failed.", err);
	            console.log("Happy panda :-) commit() was successful.");
	        });
		    // or transaction.rollback()
	        
	    });
	});

});

router.get('/droptbl', function(req, res, next) {

	// var params = req.body; -insert if param is available
	 db.serialize(function(err) {
	    db.run("DROP TABLE users;");
	    if(err)
       	 throw err;
    	else res.json({message: "The table has been truncated"});
	  });
  
    console.log(res);
});



module.exports = router;