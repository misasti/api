var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('time-keeper.db');
var check;
db.serialize(function() {
  /*
   * <!-- USERS -->
   */
  db.run("drop table if exists users");
  db.run("CREATE TABLE if not exists users (employeeId INT, firstname VARCHAR(50),middlename VARCHAR(50),lastname VARCHAR(50),username VARCHAR(10),password TEXT,status TINYINT(1),lastLoggedIn DATETIME, roleId INT, dateUpdated DATETIME, profilePic TEXT)");
  // db.run("ALTER TABLE users ADD profilePic TEXT");
  // db.run("ALTER TABLE users ADD roleId TEXT");
  /*
   * <!-- LOGS -->
  */
  db.run("drop table if exists logs");
  db.run("CREATE TABLE if not exists logs (employeeId INT, logDate DATE, logTime TIME, isFlag TINYINT(1), dateUpdated TEXT)");

  /*
   * <!-- ROLES -->
   */
  db.run("drop table if exists roles");
  db.run("CREATE TABLE if not exists roles (roleName VARCHAR(50), permission TEXT, dateUpdated DATETIME)");

  db.each("SELECT rowid AS id,* FROM users", function(err, row) {
      console.log(row);
  });
  db.each("SELECT rowid AS log_id,* FROM logs", function(err, row) {
      console.log(row);
  });


});

db.close();